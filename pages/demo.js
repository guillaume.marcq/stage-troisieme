import Head from 'next/head'
import Layout from '../components/layout'
import ImgText from "../components/img-text";
import TextBloc from "../components/TextBloc";
import Link from "next/link";
import CustomCarousel from "../components/custom-carousel";
import StarWarsEffect from "../components/star-wars-effect";

const title = 'Débuter à LOL!'

export default function Demo({}) {
  return (
    <Layout home={true} name={title} siteTitle={'LOL pour les débutants'}>
      <Head>
        <title>{title}</title>
      </Head>
      <div className={'row'}>
        <div className={'col-8'}>
          <section className={'row'}>
            <div className={'col-6'}>
              <ImgText url={'/images/combat-1.jpeg'} content={''} className={'h-100'}/>
            </div>
            <div className={'col-6'}>
              <ImgText url={'/images/runes.jpeg'}
                       content={'Une nouvelle rune'}/>
            </div>
            <div className={'col-12'}>
              <CustomCarousel />
            </div>
          </section>
        </div>
        <div className={'col-4'}>
          <section className={'row'}>
            <div className={'col-12'}>
              <ImgText url={'/images/combat-2.jpeg'} content={''}/>
            </div>
            <div className={'col-12'}>
              <StarWarsEffect title={'La saison des neiges'} description={'Saison V'} content={['Pour cette nouvelle saison, de nouveaux personnages font leur apparition.',
                'Il faudra faire attention aux nouveaux équilibrages des pouvoir qui vous réserveront des surprises',
              'Il ne faut pas oublier de s\'entrainer car sinon attention à la correction qui vous attend.']} />
            </div>
          </section>
        </div>
      </div>

      <div className={'row'}>
        <div className={'col-8'}>
          <section className={'row'}>
            <div className={'col-12'}>
              <TextBloc title={'De nouveaux pouvoirs'}
                        content={'Tout le monde attendait cette nouvelle saison avec l\'apparition de nouveaux pouvoir et la modification d\'autres...'}/>
            </div>
          </section>
        </div>
        <div className={'col-4'}>
          <section className={'row'}>
            <div className={'col-12'}>
              <TextBloc title={'Et si on regardait les nouveaux combattants'}
                        content={'Avec cette nouveaux saisons, de nouveaux combattants sont disponibles que ce soit pour le combat au corps-à-corps ou à distance...'}/>
            </div>
          </section>
        </div>
      </div>
      <Link href={'/'}>Retour à l'accueil</Link>

    </Layout>
  )
}
