import Head from 'next/head'
import Layout, {siteTitle} from '../components/layout'
import ImgText from "../components/img-text";
import TextBloc from "../components/TextBloc";
import Link from "next/link";

export default function Home({}) {
  return (
    <Layout home>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <div className={'row'}>
        <div className={'col-8'}>
          <section className={'row'}>
            <div className={'col-6'}>
              <ImgText url={'/images/placeholder.png'} content={''} className={'h-100'}/>
            </div>
            <div className={'col-6'}>
              <ImgText url={'/images/placeholder.png'}
                       content={'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et ipsum mi. Etiam quam nunc, fermentum ac aliquet sit amet, fringilla vitae ligula. Morbi pretium felis felis, ac tincidunt quam eleifend ac.'}/>
            </div>
            <div className={'col-12'}>
              <ImgText url={'/images/placeholder.png'} content={''} className={'w-100'}/>
            </div>
          </section>
        </div>
        <div className={'col-4'}>
          <section className={'row'}>
            <div className={'col-12'}>
              <ImgText url={'/images/placeholder.png'} content={''}/>
            </div>
            <div className={'col-12'}>
              <ImgText url={'/images/placeholder.png'} content={''} className={'h-100'}/>
            </div>
          </section>
        </div>
      </div>

      <div className={'row'}>
        <div className={'col-8'}>
          <section className={'row'}>
            <div className={'col-12'}>
              <TextBloc title={'Title h2'}
                        content={'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et ipsum mi. Etiam quam nunc, fermentum ac aliquet sit amet, fringilla vitae ligula. Morbi pretium felis felis, ac tincidunt quam eleifend ac.'}/>
            </div>
          </section>
        </div>
        <div className={'col-4'}>
          <section className={'row'}>
            <div className={'col-12'}>
              <TextBloc title={'Title h2'}
                        content={'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et ipsum mi. Etiam quam nunc, fermentum ac aliquet sit amet, fringilla vitae ligula. Morbi pretium felis felis, ac tincidunt quam eleifend ac.'}/>
            </div>
          </section>
        </div>
      </div>
      <Link href={'/sample'}>Besoin de composants ?</Link>
      <Link href={'/demo'}>Une demo ?</Link>

    </Layout>
  )
}
