import styles from "./star-wars-effect.module.css";

export default function StarWarsEffect({title, description, content}) {
  return (
    <div className={styles.zone}>
      <div className={styles.fade}></div>

      <div className={styles.star}>
        <div className={styles.crawl}>
          <div className={styles.title}>
            <p>{description}</p>
            <h1>{title}</h1>
          </div>

          {content.map(item => <p>{item}</p>)}
        </div>
      </div>
    </div>
  )
}