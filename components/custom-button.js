import {Button} from "react-bootstrap";

export default function CustomButton({ name, color }) {
  return (
    <>
      <Button type="button" className={`btn m-1 ${color ?? 'btn-primary'}`}>{name}</Button>
    </>
  )
}