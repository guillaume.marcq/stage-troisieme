import utilStyles from "../styles/utils.module.css";
import styles from "./header.module.css";
import Image from "next/image";
import Link from "next/link";

export default function Header({home = false, siteName = "Bob"}) {
  return (
    <header className={styles.header}>
      {home ? (
        <>
          <Image
            priority
            src="/images/rpo.jpeg"
            className={utilStyles.borderCircle}
            height={100}
            width={100}
            alt={siteName}
          />
          <h1 className={utilStyles.heading2Xl}>{siteName}</h1>
        </>
      ) : (
        <>
          <Link href="/">
            <Image
              priority
              src="/images/rpo.jpeg"
              className={utilStyles.borderCircle}
              height={108}
              width={108}
              alt={siteName}
            />
          </Link>
          <h2 className={utilStyles.headingLg}>
            <Link href="/" className={utilStyles.colorInherit}>
              {siteName}
            </Link>
          </h2>
        </>
      )}
    </header>
  )
}